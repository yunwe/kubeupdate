import os
import time
import logging
import requests
import threading
from waitress import serve
from logging.config import dictConfig
from flask import Flask, request, jsonify
from kubernetes import client, config

# 定义handler的输出格式
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')

app = Flask(__name__)

""" 日志格式配置 """
dictConfig({
    'version': 1,
    'formatters': {'default': {
        'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s',
    }},
    'handlers': {'wsgi': {
        'class': 'logging.StreamHandler',
        'stream': 'ext://flask.logging.wsgi_errors_stream',
        'formatter': 'default'
    }},
    'root': {
        'level': 'DEBUG',
        'handlers': ['wsgi']
    }
})


@app.route("/")
def main():
    return "Please see the detailed documentation: https://gitee.com/yunwe/kubeupdate.git"


def test():
    """
    Kubernetes权限测试用例
    :return: True if successful, False otherwise
    """
    try:
        config.load_incluster_config()
    except Exception as e:
        logging.error(f"Failed to load in-cluster configuration: {e}")
        return False

    v1 = client.CoreV1Api()
    logging.info("This is a test case that lists the kube-system pods:")
    try:
        pods = v1.list_namespaced_pod('kube-system')
        for pod in pods.items:
            logging.info(pod.metadata.name)
        return True
    except Exception as e:
        logging.error(f"Failed to list pods in kube-system namespace: {e}")
        return False


def send_ding_ding(resource_name: str, status: str):
    """
    资源状态就绪信息推送至钉钉
    :param resource_name: 资源名称
    :param status: 资源状态
    :return 0: 推送失败
    """
    # 获取环境变量
    id = os.getenv('id')
    key = os.getenv('key')
    phones = os.getenv('phone')
    headers = {'Content-Type': 'application/json;charset=utf-8'}

    if not key or not id:
        logging.error("key or id is empty")
        return 0

    api = 'https://oapi.dingtalk.com/robot/send?access_token=' + key
    phone_at = ', '.join([f"@{num}" for num in phones.split(',')]) if phones else ""

    try:
        body = {
            "msgtype": "markdown",
            "markdown": {
                "title": f"资源更新状态 {resource_name}",
                "text": f"{resource_name}的状态为：{status} {phone_at.rstrip(',')}"
            },
            "at": {
                "atMobiles": [num.strip() for num in phones.split(',')] if phones else [],
                "isAtAll": False
            }
        }
    except Exception as e:
        logging.error(f"Failed to construct message body: {e}")
        return 0

    response = requests.post(api, json=body, headers=headers).json()

    if response.get('errmsg') == 'ok':
        logging.info("Message push successful")
        return 1
    else:
        logging.error(f"Message push failed: {response}")
        return 0


def get_api_client(resource_type: str):
    """
    获取Kubernetes API客户端
    :param resource_type: 资源类型
    :return: API客户端实例
    """
    # 加载集群配置
    config.load_incluster_config()

    api_mapping = {
        'deployment': client.AppsV1Api,
        'stateful_set': client.AppsV1Api,
        'daemon_set': client.AppsV1Api,
        'cron_job': client.BatchV1Api,
        'job': client.BatchV1Api,
    }

    if resource_type not in api_mapping:
        raise ValueError('Invalid resource type')

    return api_mapping[resource_type]()


def get_resource_status(namespace: str, resource_type: str, resource_name: str):
    """
    获取当前资源状态，将状态信息推送至钉钉。
    :param namespace: 资源所在的命名空间
    :param resource_type: 资源类型
    :param resource_name: 资源名称
    :return:
    """
    try:
        api = get_api_client(resource_type)
        core_v1 = client.CoreV1Api()

        while True:
            try:
                # 获取资源对象
                resource_method = getattr(api, f'read_namespaced_{resource_type}')
                current = resource_method(resource_name, namespace)

                # 获取 Pod 列表
                if resource_type == 'deployment':
                    label_selector = ",".join([f"{k}={v}" for k, v in current.spec.selector.match_labels.items()])
                    pods = core_v1.list_namespaced_pod(namespace=namespace, label_selector=label_selector)
                else:
                    # 对于其他资源类型，直接获取其 Pod 列表
                    pods = core_v1.list_namespaced_pod(namespace=namespace, label_selector=f"app={resource_name}")

                # 检查每个 Pod 中的容器状态
                all_running = True
                for pod in pods.items:
                    logging.info(f"Checking Pod: {pod.metadata.name}")
                    for container_status in pod.status.container_statuses:
                        state = container_status.state
                        container_name = container_status.name
                        logging.info(f"Container: {container_name}")

                        if state.waiting is not None:
                            reason = state.waiting.reason
                            logging.info(f"State: Waiting, Reason: {reason}")
                            if reason not in ['ContainerCreating', 'PodInitializing']:
                                status = reason
                                logging.info(f"Sending message for Pod {pod.metadata.name}: {status}")
                                push_status = send_ding_ding(resource_name, status)
                                if push_status == 0:
                                    return
                                all_running = False
                                break
                        elif state.terminated is not None:
                            logging.info(f"State: Terminated, Exit Code: {state.terminated.exit_code}")
                            status = 'Terminated'
                            logging.info(f"Sending message for Pod {pod.metadata.name}: {status}")
                            push_status = send_ding_ding(resource_name, status)
                            if push_status == 0:
                                return
                            all_running = False
                            break
                        elif state.running is not None:
                            logging.info(f"State: Running")
                        else:
                            logging.info(f"State: Unknown")
                            status = 'Unknown'
                            logging.info(f"Sending message for Pod {pod.metadata.name}: {status}")
                            push_status = send_ding_ding(resource_name, status)
                            if push_status == 0:
                                return
                            all_running = False
                            break

                    if not all_running:
                        break  # 如果发现异常状态，退出当前 Pod 的循环

                # 如果所有容器都处于运行状态，发送成功消息
                if all_running:
                    status = 'Available'
                    logging.info(f"All containers are running, sending message: {status}")
                    push_status = send_ding_ding(resource_name, status)
                    if push_status == 0:
                        return
                    break  # 退出循环

            except client.ApiException as e:
                logging.error(f'Failed to obtain resources: {resource_name} {e.reason}')
                break  # 如果获取失败，退出循环

            # 等待30秒后再次检查
            time.sleep(30)

    except ValueError as e:
        logging.error(str(e))


@app.route('/update_image', methods=['POST'])
def update_image():
    """
    处理请求的JSON，用于更新资源镜像
    :return:
    """
    # 解析请求数据
    data = request.get_json()
    resource_name = data['name']
    namespace = data['namespace']
    image = data['image']
    resource_type = data['resource_type']
    token = data['token']
    # 获取容器名称，如果不存在则返回None
    container_name = data.get('container', None)
    default_token = 'PXhNVVgMDdWKCrvPHOlwGseVMBscEXJKmAcrxltVQtTeNJQrMBMEIqUWSKJulDaFUUgy'
    # 获取环境变量，配置默认token
    env_token = os.getenv('TOKEN', default_token)
    logging.info("Default token: ", default_token)
    # 验证 token
    if token != env_token:
        return jsonify({'error': 'Invalid Token'})

    try:
        api = get_api_client(resource_type)
        resource_method = getattr(api, f'read_namespaced_{resource_type}')
        current = resource_method(resource_name, namespace)
    except (client.ApiException, ValueError) as e:
        return jsonify({'error': str(e)})

    for container in current.spec.template.spec.containers:
        if container_name is None or container.name == container_name:
            container.image = image

    try:
        patch_method = getattr(api, f'patch_namespaced_{resource_type}')
        result = patch_method(resource_name, namespace, current)
    except client.ApiException as e:
        return jsonify({'error': e.reason, 'message': e.body})

    logging.debug(result.to_dict() if result else None)
    # 获取push环境变量，默认为disable
    push_enabled = os.getenv('push', 'disable')
    if push_enabled.lower() == 'enable':
        # 等待5秒后开始检查
        time.sleep(5)
        # 异步执行
        threading.Thread(target=get_resource_status, args=(namespace, resource_type, resource_name)).start()

    return jsonify({'message': 'Image update successful'})


if __name__ == '__main__':
    success = test()
    if success:
        logging.info("The Kubeupdate app is up and running!")
    else:
        print("Check that ClusterRole, ServiceAccount, and ClusterRoleBinding are authorized correctly")
    serve(app, host='0.0.0.0', port=5000)
