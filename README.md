## kubeupdate

### 介绍
通过POST请求更新Kubernetes资源，**支持Kubernetes 1.20.15 ~~ Kubernetes 1.28.0**，其他版本未测试。

## 安装部署

> 项目必须部署到Kubernetes集群里！！


### 镜像选择
#### 自行构建镜像

```shell
git clone https://gitee.com/yunwe/kubeupdate.git
cd kubeupdate
docker build -t harbor.com/kubeupdate:0.4.12 . && docker push harbor.com/kubeupdate:0.4.12
```

#### 使用已有镜像

```
registry.cn-hangzhou.aliyuncs.com/huang-image/kubeupdate:0.4.12
```

### 部署Kubeupdate
除了deployment的yaml文件外，其他的都是权限获取和绑定，没有绑定集群角色将无法修改资源

```yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: kubeupdate-role
rules:
- apiGroups: [""]
  resources: ["pods"]
  verbs: ["get", "watch", "list"]
- apiGroups: ["apps", "batch"]
  resources: ["deployments", "statefulsets", "jobs", "cronjobs", "daemonsets"]
  verbs: ["get", "list", "watch", "create", "update", "patch", "delete"]
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: kubeupdate-rolebinding
subjects:
- kind: ServiceAccount
  # 绑定 kubeupdate 角色
  name: kubeupdate
  namespace: kube-system
roleRef:
  kind: ClusterRole
  # cluster-admin拥有最高权限，需要权限收缩可以换成 kubeupdate-role
  name: kubeupdate-role
  apiGroup: rbac.authorization.k8s.io
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: kubeupdate
  namespace: kube-system
  labels:
    kubernetes.io/cluster-service: "true"
    addonmanager.kubernetes.io/mode: Reconcile
---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: kubeupdate
  name: kubeupdate
  namespace: kube-system
spec:
  progressDeadlineSeconds: 600
  replicas: 1
  revisionHistoryLimit: 10
  selector:
    matchLabels:
      app: kubeupdate
  template:
    metadata:
      labels:
        app: kubeupdate
    spec:
      serviceAccountName: kubeupdate
      containers:
      - image: registry.cn-hangzhou.aliyuncs.com/huang-image/kubeupdate:0.4.12
        imagePullPolicy: Always
        name: kubeupdate
        ports:
        - containerPort: 5000
          name: 5000tcp2
          protocol: TCP
        # 自定义Token，防止非法请求
        env:
          - name: TOKEN
            value: 'PXhNVVVgMDdWKCrvPHOlwGseVMBscEXJKmAcrxltVQtTeNJQrMBMEIqUWSKJulDaFUUgy'          
      dnsPolicy: ClusterFirst
      restartPolicy: Always
      schedulerName: default-scheduler
      securityContext: {}
      terminationGracePeriodSeconds: 30
---
apiVersion: v1
kind: Service
metadata:
  labels:
    app: kubeupdate
  name: kubeupdate
  namespace: kube-system
spec:
  externalTrafficPolicy: Cluster
  ports:
  - nodePort: 30002
    port: 5000
    protocol: TCP
    targetPort: 5000
  selector:
    app: kubeupdate
  sessionAffinity: None
  type: NodePort
```

## 使用说明
### 部署测试用例
先部署一个测试的应用
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    app: nginx
spec:
  replicas: 1
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.20
        ports:
        - containerPort: 80
      - name: tomcat
        image: tomcat:9.0
        ports:
        - containerPort: 8080
```
### 调用接口更新
上面的Pod有多个容器，必须添加一个`container`参数，指定容器名称：
```shell script
curl -X POST http://192.168.1.38:30002/update_image -H "Content-Type: application/json" -d '{
    "name": "nginx-deployment",
    "namespace": "default",
    "image": "tomcat:9-jdk11",
    "resource_type": "deployment",
    "container": "tomcat",
    "token": "PXhNVVVgMDdWKCrvPHOlwGseVMBscEXJKmAcrxltVQtTeNJQrMBMEIqUWSKJulDaFUUgy"
}'
```
如果只有一个容器，则不需要指定：

```shell
curl -X POST http://192.168.1.38:30002/update_image -H "Content-Type: application/json" -d '{
    "name": "nginx-deployment",
    "namespace": "default",
    "image": "nginx:1.22",
    "resource_type": "deployment",
    "token": "PXhNVVVgMDdWKCrvPHOlwGseVMBscEXJKmAcrxltVQtTeNJQrMBMEIqUWSKJulDaFUUgy"
}'
```


参数说明
| 参数          | 说明                                                         | 是否必须 |
| ------------- | ------------------------------------------------------------ | -------- |
| name          | 资源名称                                                     | 是       |
| namespace     | 命名空间名称                                                 | 是       |
| image         | 镜像地址                                                     | 是       |
| resource_type | 资源类型，支持 deployment \| stateful_set \| daemon_set \| job \| cron_job，资源类型的名称不能错! | 是       |
| container     | 容器名称，pod如果有多个容器，则必须添加                      | 否       |
| token         | 携带Token，上面是默认的，可以通过环境变量的形式注入Token     | 是       |