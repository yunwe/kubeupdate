FROM swr.cn-north-4.myhuaweicloud.com/ddn-k8s/docker.io/python:3.12-slim
ENV TZ=Asia/Shanghai
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN useradd kubeupdate
WORKDIR /kubeupdate/
COPY app.py Dockerfile requirements.txt /kubeupdate/
RUN pip3 install -i https://mirrors.ustc.edu.cn/pypi/web/simple -r requirements.txt
RUN chown -R kubeupdate:kubeupdate /kubeupdate/
USER kubeupdate
EXPOSE 5000
CMD ["python","app.py"]